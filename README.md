# An attempt to estimate IPO gains at the lock-up expiration date
### 2021-01-21 by Ed Parsadanyan

## Description

This program merges a list of IPO from iposcoop.com with the list of IPO lock-up expiration dates from marketbeat.com

Resulting spreadsheet can be found here:

https://docs.google.com/spreadsheets/d/1dj-8T59XvWnAkOKw4H_KgIHCIk_hkSFHWvdVrFQBsrM/edit?usp=sharing

## Methodology

list of IPO offerings since 2000

* https://www.iposcoop.com/scoop-track-record-from-2000-to-present/


list of IPO expiration dates:

* http://web.archive.org/web/20191106105740/https://www.marketbeat.com/ipos/lockup-expirations/

* http://web.archive.org/web/20200227091132/https://www.marketbeat.com/ipos/lockup-expirations/

* http://web.archive.org/web/20200411035622/https://www.marketbeat.com/ipos/lockup-expirations/

* http://web.archive.org/web/20200621161429/https://www.marketbeat.com/ipos/lockup-expirations/

* http://web.archive.org/web/20200718020604/https://www.marketbeat.com/ipos/lockup-expirations/

* http://web.archive.org/web/20201004155919/https://www.marketbeat.com/ipos/lockup-expirations/

Web-Archive pages were parsed using https://stats-consult.shinyapps.io/shinyparser_poc/

Parsing configuration is saved in yml files along with the data. Two files are provided as the archive page from 2019 was in slightly different format


## Known Issues

Not all tickers are correctly recognized by Googlefinance() formula
