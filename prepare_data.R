# Check IPO profitability from initial offering to the lock-up expiration date
# 2020-01-20 by Ed Parsadanyan
# 
# INTRO:
# 
# list of IPO offerings since 2000
# https://www.iposcoop.com/scoop-track-record-from-2000-to-present/
# 
# 
# list of IPO expiration dates:
# http://web.archive.org/web/20191106105740/https://www.marketbeat.com/ipos/lockup-expirations/
# http://web.archive.org/web/20200227091132/https://www.marketbeat.com/ipos/lockup-expirations/
# http://web.archive.org/web/20200411035622/https://www.marketbeat.com/ipos/lockup-expirations/
# http://web.archive.org/web/20200621161429/https://www.marketbeat.com/ipos/lockup-expirations/
# http://web.archive.org/web/20200718020604/https://www.marketbeat.com/ipos/lockup-expirations/
# http://web.archive.org/web/20201004155919/https://www.marketbeat.com/ipos/lockup-expirations/
# 
# # Web-Archive pages were parsed using https://stats-consult.shinyapps.io/shinyparser_poc/
# Parsing configuration is saved in yml files along with the data.
# Two files are provided as the archive page from 2019 was in slightly different format



# > sessionInfo()
# R version 3.5.3 (2019-03-11)
# Platform: x86_64-w64-mingw32/x64 (64-bit)
# Running under: Windows 10 x64 (build 17763)
# 
# Matrix products: default
# 
# locale:
# [1] LC_COLLATE=English_United States.1251  LC_CTYPE=English_United States.1251    LC_MONETARY=English_United States.1252
# [4] LC_NUMERIC=C                           LC_TIME=English_United States.1252    
# 
# attached base packages:
# [1] stats     graphics  grDevices utils     datasets  methods   base     
# 
# other attached packages:
# [1] openxlsx_4.1.0       readxl_1.3.1         data.table_1.13.6    stringi_1.4.3        stringr_1.4.0        tidyr_0.8.3          dplyr_0.8.0.1       
# [8] RevoUtils_11.0.3     RevoUtilsMath_11.0.0
# 
# loaded via a namespace (and not attached):
# [1] Rcpp_1.0.1       crayon_1.3.4     assertthat_0.2.1 cellranger_1.1.0 R6_2.3.0         magrittr_1.5     zip_2.1.1        pillar_1.3.1    
# [9] rlang_0.4.10     rstudioapi_0.10  tools_3.5.3      glue_1.3.1       purrr_0.3.2      compiler_3.5.3   pkgconfig_2.0.2  tidyselect_0.2.5
# [17] tibble_2.1.1    




# Set locale if needed
Sys.setlocale("LC_CTYPE", ".1251")
Sys.setlocale("LC_COLLATE", ".1251")

pkgs <- c("dplyr", "tidyr", "stringr", "stringi", "data.table","readxl","openxlsx")
sapply(pkgs, require, character.only = TRUE)




#################################
### 1. Put CSV files together ###
#################################

folder      <- "data/"

# Create a single dataframe from a list of csv files
csv_fnames <- list.files(path = folder, pattern="*.csv")
csv_fnames <- str_c(folder,csv_fnames)

# Combine 2020 data separately from 2019 as the csv format is different
csv_fnames_2019 <- grep("2019",csv_fnames, value = TRUE)
csv_fnames_2020 <- grep("2020",csv_fnames, value = TRUE)

raw_df_2019     <- rbindlist(lapply(csv_fnames_2019, read.csv, encoding="UTF-8"))
raw_df_2020     <- rbindlist(lapply(csv_fnames_2020, read.csv, encoding="UTF-8"))


# Clean 2019 data
raw_expiration_dates <- raw_df_2019 %>% mutate(
  COMPANY = word(COMPANYTICKER,1,1,sep="\\("),
  TICKER  = gsub("[\\(\\)]", "",str_match(COMPANYTICKER,"\\(.*?\\)"))
) %>%
# Append 2020 and clean duplicates
  select(-one_of(c("COMPANYTICKER"))) %>% bind_rows(raw_df_2020) %>% arrange(TICKER) %>% distinct() %>% 
# Format date and price variables
  mutate(
    INITIAL_PRICE = as.double(gsub('[$]', '', OFFER)),
    IPO_DATE      = as.Date(DATE, format="%m/%d/%Y"),
    LOCKUP_DATE   = as.Date(EXPIRATION, format="%m/%d/%Y"),
) %>% 
# filter lock-up dates and keep 2020 only
  filter(year(LOCKUP_DATE)==2020) %>% arrange(IPO_DATE)




##############################
### 2. Import IPO database ###
##############################

lockup_ <- 180
exl_convertdate <- "1899-12-30"
  
raw_IPOdb <- read_xls(str_c(folder,"SCOOP-Rating-Performance.xls"),skip = 35)

# Remove table headers for previous years (2019, 2018 etc)
IPOdb <- raw_IPOdb %>% filter(!is.na(Symbol) & toupper(Symbol)!=toupper("Symbol")) %>% 
# Convert Excel dates and clean some variables
  mutate(
  IPO_DATE = as.Date(as.numeric(Date), origin = exl_convertdate),
  TICKER   = copy(Symbol)
  # TICKER   = gsub("\\.","",copy(Symbol))
) %>% 
# filter IPO dates and keep 2019-2020 only
filter(year(IPO_DATE) %in% c(2019,2020)) %>% arrange(IPO_DATE)




################################################
### 3. Merge IPO database with lock-up dates ###
################################################


IPOdb_merged <- IPOdb %>% merge(raw_expiration_dates, by=c("IPO_DATE","TICKER"), all=TRUE) %>% 
# Some additional cleaning is required due to record mismatches and missing lock-up dates in the csv files
  mutate(
    COMPANY       =ifelse(!is.na(Issuer),Issuer,COMPANY),
    INITIAL_PRICE =ifelse(!is.na(INITIAL_PRICE),INITIAL_PRICE,as.double(Price...5)),
    LOCKUP_IMPUTED=ifelse(!is.na(LOCKUP_DATE), LOCKUP_DATE,
                          IPO_DATE+lockup_),
    LOCKUP_IMPUTED=as.Date(LOCKUP_IMPUTED, origin = "1970-01-01"),
    LOCKUP_PERIOD = LOCKUP_DATE - IPO_DATE
  ) %>% 
# Remove SPAC IPOs
  filter(!grepl(toupper("acquisition"),toupper(COMPANY)))

# Keep only IPOs with lockup expiration dates in 2020
IPOdb_2020 <- IPOdb_merged %>% filter(year(LOCKUP_IMPUTED) %in% c(2020)) %>% arrange(IPO_DATE) %>% 
  select(one_of(c("TICKER","COMPANY","INITIAL_PRICE","IPO_DATE","LOCKUP_IMPUTED"))) %>% distinct()


write.xlsx(IPOdb_2020,file="IPOdb_2020.xlsx")



